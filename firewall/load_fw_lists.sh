#!/bin/sh

source /etc/localfw.conf

ipset=/usr/sbin/ipset
ipset_restore="/usr/sbin/ipset -exist restore"
#ipset="/bin/echo ipset"
#ipset_restore=/bin/cat

echo -n "Loading firewall blocklists: "

for list in $BLOCKLISTS;
do
  echo -n " ${list}"
  case ${list} in
    SPAMDROP|SPAMEDROP|DSHIELD|BOGON|RBN)
      listtype="hash:net"
      ;;
    TOR|CIARMY|BFB|OPENBL|AUTOSHUN)
      listtype="hash:ip"
      ;;
  esac
  
  $ipset -exist create ${list} ${listtype}
  
  if [ "$1" != "createonly" ]; then
    $ipset -exist create ${list}_new ${listtype}
    $ipset flush ${list}_new

    case ${list} in
      SPAMDROP)
        wget -q -O - "http://www.spamhaus.org/drop/drop.lasso" \
            | awk '/^[^;]/ {print "add SPAMDROP_new "$1}' | ${ipset_restore}
        ;;
      SPAMEDROP)
        wget -q -O - "http://www.spamhaus.org/drop/edrop.lasso" \
            | awk '/^[^;]/ {print "add SPAMEDROP_new "$1}' | ${ipset_restore}
        ;;
      DSHIELD)
        wget -q -O - "http://feeds.dshield.org/block.txt" \
            | awk '/^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/ {print "add DSHIELD_new "$1"/"$3}' | ${ipset_restore}
        ;;
      BOGON)
        wget -q -O - "http://www.team-cymru.org/Services/Bogons/bogon-bn-agg.txt" \
            | awk '/^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/ {print "add BOGON_new "$1}' | ${ipset_restore}
        ;;
      RBN)
        wget -q -O - "http://rules.emergingthreats.net/blockrules/rbn-ips.txt" \
            | awk -F "/" '/^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/ {prefixlen=($2==""?32:$2); print "add RBN_new "$1"/"prefixlen}' | ${ipset_restore}
        ;;
      TOR)
        wget -q -O - "https://check.torproject.org/cgi-bin/TorBulkExitList.py?ip=1.1.1.1" \
            | awk '/^[^#]/ {print "add TOR_new "$1}' | ${ipset_restore}
        ;;
      CIARMY)
        wget -q -O - "http://www.ciarmy.com/list/ci-badguys.txt" \
            | awk '/^[^#]/ {print "add CIARMY_new "$1}' | ${ipset_restore}
        ;;
      BFB)
        wget -q -O - "http://danger.rulez.sk/projects/bruteforceblocker/blist.php" \
            | awk '/^[^#]/ {print "add BFB_new "$1}' | ${ipset_restore}
        ;;
      OPENBL)
        wget -q -O - "http://www.us.openbl.org/lists/base_30days.txt" \
            | awk '/^[^#]/ {print "add OPENBL_new "$1}' | ${ipset_restore}
        ;;
      AUTOSHUN)
        wget -q -O - "http://www.autoshun.org/files/shunlist.csv" \
            | awk -F , '/^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/ {print "add AUTOSHUN_new "$1}' | ${ipset_restore}
        ;;
    esac

    $ipset swap ${list}_new ${list}
    $ipset destroy ${list}_new
  fi
done

echo "."
