#!/usr/bin/python

debug=False

import sys
import os
import time
import csv
import netaddr
import httplib
import cStringIO
import zipfile
import subprocess
from datetime import datetime, timedelta

execfile("/etc/localfw.conf")

#Update database
db_file="/var/lib/geoip/GeoIPCountryWhois.csv"
#http://geolite.maxmind.com/download/geoip/database/GeoIPCountryCSV.zip
db_src_host="geolite.maxmind.com"
db_src_path="/download/geoip/database/GeoIPCountryCSV.zip"
filedate=datetime.utcfromtimestamp(0)
if os.path.exists(db_file):
  filedate=datetime.utcfromtimestamp(os.path.getmtime(db_file))

if filedate < datetime.now()-timedelta(days=1):
  conn = httplib.HTTPConnection(db_src_host)
  conn.request("HEAD", db_src_path)
  res = conn.getresponse()
  url_date = datetime.strptime(res.getheader('last-modified'), "%a, %d %b %Y %H:%M:%S %Z")
  conn.close()
  if url_date > filedate:
    print "Updating GeoIP database:" ,
    conn.connect()
    conn.request("GET", db_src_path)
    f = conn.getresponse()
    m = cStringIO.StringIO(f.read())
    f.close()
    conn.close()
    z = zipfile.ZipFile(m,'r');
    z.extractall("/var/lib/geoip/")
    z.close()
    m.close()
    print "Done."

countries=set([cc.upper() for cc in (CC_DENYPORTS_COUNTRIES + " " + CC_ALLOWPORTS_COUNTRIES).split(' ')]);

f=open(db_file);
linereader= csv.reader(f);
ccranges= [(row[4], netaddr.iprange_to_cidrs(row[0],row[1])) for row in linereader if row[4] in countries]
f.close();

commands= ["add CC_%s_new %s" % (cc, net) for (cc,nets) in ccranges for net in nets]

print "Loading country lists:" ,

if debug:
  ipset="/bin/echo"
else:
  ipset="/usr/sbin/ipset"

for cc in countries:
  subprocess.call([ipset, "-exist", "create", "CC_" + cc, "hash:net"]);
  subprocess.call([ipset, "-exist", "create", "CC_" + cc + "_new", "hash:net"]);
  subprocess.call([ipset, "flush", "CC_" + cc + "_new"]);

if debug:
  p=subprocess.Popen(["/bin/cat"], stdin=subprocess.PIPE)
else:
  p=subprocess.Popen([ipset, "restore"], stdin=subprocess.PIPE)
p.communicate("\n".join(commands) + "\n");

for cc in countries:
  print cc  ,
  subprocess.call([ipset, "swap", "CC_" + cc, "CC_" + cc + "_new"]);
  subprocess.call([ipset, "destroy", "CC_" + cc + "_new"]);

print "."
