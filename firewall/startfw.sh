#!/bin/sh

source /etc/localfw.conf

iptables=/sbin/iptables
ipset=/usr/sbin/ipset
exe_path=/usr/local/bin

#iptables="/bin/echo iptables"
#ipset="/bin/echo iptables"

#Blocklists
${exe_path}/load_fw_lists.sh createonly

for bl in ${BLOCKLISTS}; do
  $iptables -A LOCALINPUT -m set --match-set ${bl} src -j DROP
done

${exe_path}/load_fw_lists.sh &

#CC Lists
$iptables -N CC_DENYPORTS
$iptables -N CC_ALLOWPORTS

for cc in ${CC_DENYPORTS_COUNTRIES};
do
  $ipset -exist create CC_${cc} hash:net
  $iptables -A CC_DENYPORTS -m set --match-set CC_${cc} src -j DROP
done

for cc in ${CC_ALLOWPORTS_COUNTRIES};
do
  $ipset -exist create CC_${cc} hash:net
  $iptables -A CC_ALLOWPORTS -m set --match-set CC_${cc} src -j ACCEPT
done

${exe_path}/load_fw_cclists.py &

for action in DENY ALLOW; do
  for proto in TCP UDP; do
    $ipset -exist create CC_${action}PORTS_${proto} bitmap:port range 0-65535

    $ipset -exist create CC_${action}PORTS_${proto}_new bitmap:port range 0-65535
    $ipset flush CC_${action}PORTS_${proto}_new
    for port in $(eval echo \$CC_${action}PORTS_${proto});
    do
      $ipset add CC_${action}PORTS_${proto}_new $port;
    done 
    $ipset swap CC_${action}PORTS_${proto} CC_${action}PORTS_${proto}_new
    $ipset destroy CC_${action}PORTS_${proto}_new

    $iptables -A ${TARGETCHAIN} -p ${proto} -m set --match-set CC_${action}PORTS_${proto} dst -j CC_${action}PORTS
  done
done
