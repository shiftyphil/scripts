#!/usr/local/bin/python2

import getopt, sys
import libzfs
from datetime import datetime
import periods

config_prefix = "au.id.pjh.autosnap:"

num_yearly=-1
num_quarterly=4
num_monthly=3
num_weekly=4
num_daily=7
num_hourly=24

day_rollover_offset=0

quiet=False
verbose=False
dryrun=False

def is_autosnapshot(name):
    return name.find('@autosnap-') > -1

def get_creation_datetime(snap):
    return datetime.fromtimestamp(float(snap.properties["creation"].rawvalue))
    
def prune_dataset(dataset):
    if not quiet: print "Checking \"{0}\"".format(dataset.name)
    frequency = "hourly"
    if dataset.properties.has_key(config_prefix + "frequency"):
        frequency = dataset.properties[config_prefix + "frequency"].value
    freq_id = { "hourly": 1, "daily": 2, "weekly": 3, "monthly": 4, "quarterly": 5, "yearly": 6 }.get(frequency, 1)

    all_snapshots = dataset.snapshots
    snapshots = [x for x in all_snapshots if is_autosnapshot(x.name)]

    if len(snapshots) == 0:
        print "ERR: No snapshots found"
        return

    snap_dates = [get_creation_datetime(snap) for snap in snapshots]
    snap_dates.sort()

    counts = []
    save_dates = set()
    if freq_id <= 1:
        hourlies = periods.get_hourlies(snap_dates, num_hourly)
        counts.append("{0} hourly".format(len(hourlies)))
        save_dates |= hourlies
    if freq_id <= 2:
        dailies = periods.get_dailies(snap_dates, num_daily)
        counts.append("{0} daily".format(len(dailies)))
        save_dates |= dailies
    if freq_id <= 3:
        weeklies = periods.get_weeklies(snap_dates, num_weekly)
        counts.append("{0} weekly".format(len(weeklies)))
        save_dates |= weeklies
    if freq_id <= 4:
        monthlies = periods.get_monthlies(snap_dates, num_monthly)
        counts.append("{0} monthly".format(len(monthlies)))
        save_dates |= monthlies
    if freq_id <= 5:
        quarterlies = periods.get_quarterlies(snap_dates, num_quarterly)
        counts.append("{0} quarterly".format(len(quarterlies)))
        save_dates |= quarterlies
    if freq_id <= 6:
        yearlies = periods.get_yearlies(snap_dates, num_yearly)
        counts.append("{0} yearly".format(len(yearlies)))
        save_dates |= yearlies

    if not quiet:
        print "Preserving {0} snapshots - {1}".format(len(save_dates),", ".join(counts))

    if verbose:
        snapshots.sort(key=lambda x: x.properties["creation"].rawvalue)
        for x in snapshots:
	    time = get_creation_datetime(x)
	    tags = []
	    if freq_id <= 6 and time in yearlies: tags.append('yearly')
	    if freq_id <= 5 and time in quarterlies: tags.append('quarterly')
	    if freq_id <= 4 and time in monthlies: tags.append('monthly')
	    if freq_id <= 3 and time in weeklies: tags.append('weekly')
	    if freq_id <= 2 and time in dailies: tags.append('daily')
	    if freq_id <= 1 and time in hourlies: tags.append('hourly')
	    if len(tags) > 0:
		print "\"{0}\" - {1} -> KEEP ({2})".format(x.name, time, ", ".join(tags))
	    else:
		print "\"{0}\" - {1} -> DELETE".format(x.name, time)

    to_delete = [x for x in snapshots if get_creation_datetime(x) not in save_dates]

    for x in to_delete:
        if not quiet: print "Removing snapshot \"{0}\" -  Created at: {1}".format(x.name, get_creation_datetime(x))
        if not dryrun: x.delete(False)

def main():
    global verbose, quiet, dryrun
    auto = False
    recurse = False
    try:
        opts, args = getopt.getopt(sys.argv[1:], "anqrv", ["auto", "dryrun", "quiet", "recurse", "verbose"])
    except getopt.GetoptError as err:
        # print help information and exit:
        print str(err)  # will print something like "option -a not recognized"
        #usage()
        sys.exit(2)
    for o, a in opts:
        if o in ("-v", "--verbose"):
            verbose = True
        elif o in ("-q", "--quiet"):
            quiet = True
        elif o in ("-n", "--dryrun"):
            dryrun = True
        elif o in ("-a", "--auto"):
            auto = True
        elif o in ("-r", "--recurse"):
            recurse = True
        else:
            assert False, "unhandled option"

    if auto:
       for dataset in libzfs.ZFS().datasets:
            if dataset.properties.has_key(config_prefix + "prune"):
                prop = dataset.properties[config_prefix + "prune"]
                if prop.value == "on" and (recurse or prop.source == libzfs.PropertySource.LOCAL):
                    prune_dataset(dataset)
    else:
        for dataset_name in args:
            dataset = libzfs.ZFS().get_dataset(dataset_name)
            prune_dataset(dataset)
            if recurse:
               for child in dataset.children_recursive:
                    prune_dataset(child)

if __name__ == "__main__":
    main();
