#!/usr/local/bin/python2

import os, getopt, sys
from datetime import datetime
import libzfs

config_prefix="au.id.pjh.shadowcopy:"

verbose = False
quiet = False
dryrun = False

def get_creation_datetime(snap):
    return datetime.utcfromtimestamp(float(snap.properties["creation"].rawvalue))

def setup_shadows(dataset):
    if not quiet: print "Setting up shadow copies on \"{0}\"".format(dataset.name)
    mountpoint = dataset.mountpoint
    if mountpoint is None:
        return

    shadowpath = os.path.join(mountpoint, ".shadowcopy")
    snappath = os.path.join(mountpoint, ".zfs/snapshot")

    #Make sure snapshot mountpoint is resolved
    for snap in os.listdir(snappath):
        os.path.exists(os.path.join(snappath, snap))

    if not os.path.exists(shadowpath):
        os.mkdir(shadowpath)

    os.chdir(shadowpath)
    for link in os.listdir("."):
        if os.path.islink(link) and not os.path.exists(link):
            if not quiet: print "Removing dead link \"{0}\"".format(link)
            if not dryrun: os.remove(link)

    existing = os.listdir(".")
    for snap in dataset.snapshots:
        snap_time = get_creation_datetime(snap)
        snap_link = snap_time.strftime("@GMT-%Y.%m.%d-%H.%M.%S")
        if snap.mountpoint is None:
            continue
        if snap_link in existing:
            if os.readlink(snap_link) == snap.mountpoint:
                if verbose: print "Ignoring existing link \"{0}\"".format(snap_link)
                continue
            if not quiet: print "Removing incorrect link \"{0}\"".format(snap_link)
            if not dryrun: os.remove(snap_link)
        if not quiet: print "Linking \"{0}\" to \"{1}\"".format(snap.mountpoint, snap_link)
        if not dryrun: os.symlink(snap.mountpoint, snap_link)

def main():
    global verbose, quiet, dryrun
    auto = False
    recurse = False
    try:
        opts, args = getopt.getopt(sys.argv[1:], "anqrv", ["auto", "dryrun", "verbose", "recurse", "quiet"])
    except getopt.GetoptError as err:
        print str(err)
        #usage()
        sys.exit(2)
    for o, a in opts:
        if o in ("-v", "--verbose"):
            verbose = True
        elif o in ("-q", "--quiet"):
            quiet = True
        elif o in ("-n", "--dryrun"):
            dryrun = True
        elif o in ("-a", "--auto"):
            auto = True
        elif o in ("-r", "--recursive"):
            recurse = True
        else:
            assert False, "unhandled option"

    if auto:
       for dataset in libzfs.ZFS().datasets:
            if dataset.properties.has_key(config_prefix + "enabled"):
                prop = dataset.properties[config_prefix + "enabled"]
                if prop.value == "on" and (recurse or prop.source == libzfs.PropertySource.LOCAL):
                    setup_shadows(dataset)
    else:
        for dataset_name in args:
            dataset = libzfs.ZFS().get_dataset(dataset_name)
            setup_shadows(dataset)
            if recurse:
                for child in dataset.children_recursive:
                    setup_shadows(child)

if __name__ == "__main__":
    main();
