#!/bin/sh

freq=$1

if [ -z $1 ]; then exit 1; fi

snapname=`/bin/date +"autosnap-%Y.%m.%d-%H.%M.%S-%Z"`

/sbin/zfs get -H -t filesystem -o value,name au.id.pjh.autosnap:frequency |
    awk -v snapname="$snapname" -v pattern="$freq" -F "\t" '$1 ~ pattern {print "\""$2"@"snapname"\""}' |
    /usr/bin/xargs /sbin/zfs snap
