#!/usr/local/bin/python2

from datetime import datetime, date, time, timedelta

day_rollover_offset=0

def get_yearlies(dates, max):
    yearlies = set()
    year = date.today().year
    while max < 0 or len(yearlies) < max:
        rollover = datetime(year, 1, 1) - timedelta(hours=day_rollover_offset)
        first_snap = next((x for x in dates if x >= rollover), None)
        if first_snap <> None and first_snap not in yearlies:
	    yearlies.add(first_snap)
        if rollover < dates[0]:
            break
        year-=1
    return yearlies

def get_quarterlies(dates, max):
    quarterlies = set()
    today = date.today()
    year = today.year
    quarter = (today.month - 1) // 3
    while max < 0 or len(quarterlies) < max:
        rollover = datetime(year, quarter * 3 + 1, 1) - timedelta(hours=day_rollover_offset)
        first_snap = next((x for x in dates if x >= rollover), None)
        if first_snap <> None and first_snap not in quarterlies:
	    quarterlies.add(first_snap)
        if rollover < dates[0]:
            break
        quarter -= 1
        if quarter < 0:
            quarter = 3
            year -= 1
    return quarterlies

def get_monthlies(dates, max):
    monthlies = set()
    today = date.today()
    year = today.year
    month = today.month
    while max < 0 or len(monthlies) < max:
        rollover = datetime(year, month, 1) - timedelta(hours=day_rollover_offset)
        first_snap = next((x for x in dates if x >= rollover), None)
        if first_snap <> None and first_snap not in monthlies:
	    monthlies.add(first_snap)
        if rollover < dates[0]:
            break
        month -= 1
        if month < 1:
            month = 12
            year -= 1
    return monthlies

def get_weeklies(dates, max):
    weeklies = set()
    today = date.today()
    rollover = datetime.combine(today, time()) - timedelta(days=today.weekday(),hours=day_rollover_offset)
    while max < 0 or len(weeklies) < max:
        first_snap = next((x for x in dates if x >= rollover), None)
        if first_snap <> None and first_snap not in weeklies:
	    weeklies.add(first_snap)
        if rollover < dates[0]:
            break
        rollover -= timedelta(weeks=1)
    return weeklies

def get_dailies(dates, max):
    dailies = set()
    rollover = datetime.combine(date.today(), time()) - timedelta(hours=day_rollover_offset)
    while max < 0 or len(dailies) < max:
        first_snap = next((x for x in dates if x >= rollover), None)
        if first_snap <> None and first_snap not in dailies:
	    dailies.add(first_snap)
        if rollover < dates[0]:
            break
        rollover -= timedelta(days=1)
    return dailies

def get_hourlies(dates, max):
    hourlies = set()
    today = datetime.now()
    rollover = datetime(today.year, today.month, today.day, today.hour, 0, 0)
    while max < 0 or len(hourlies) < max:
        first_snap = next((x for x in dates if x >= rollover), None)
        if first_snap <> None and first_snap not in hourlies:
	    hourlies.add(first_snap)
        if rollover < dates[0]:
            break
        rollover -= timedelta(hours=1)
    return hourlies
